@extends('layouts.site.master')
@section('content')

@include('layouts.site.blocks.slider')
@include('layouts.site.blocks.services')
@include('layouts.site.blocks.propertydetails')
@include('layouts.site.blocks.photogallery')
@include('layouts.site.blocks.floorplans')
@include('layouts.site.blocks.agent')
@include('layouts.site.blocks.feedback')
@include('layouts.site.blocks.service')
@include('layouts.site.blocks.contact')

@endsection