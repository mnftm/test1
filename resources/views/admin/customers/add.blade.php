@extends('layouts.admin.master')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="post" enctype="multipart/form-data" action="{{URL::action('Admin\ContentController@postAddCustomer')}}" enctype="multipart/form-data" id="rahweb_form" >
                    @include('admin.customers.form')
                    <input type="file" name="image">
                </form>
            </div>
        </div>
    </div>

@endsection