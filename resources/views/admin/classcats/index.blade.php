@extends('layouts.admin.master')
@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">دسته کلاس‌ها</h3>
                            <div class="card-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="{{URL::action('Admin\ContentController@getAddClasscat')}}"> <button type="button" class="btn btn-info">افزودن <span class="badge"></span></button></a>

                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover">
                                <tr>
                                    <th>نام</th>
                                    <th>تصوير</th>
                                    <th>ترتيب</th>
                                    <th>وضعيت</th>
                                </tr>
                                @foreach($classcats as $classcat)
                                    <tr>
                                        <td>{{$classcat->title}}</td>
                                        <td><img src="{{asset('assets/admin/uploads/medium/'.$classcat->img)}}" style="width: 25%"> </td>
                                        <td>{{$classcat->order}}</td>
                                        <td>@if($classcat->status) فعال @else غير فعال@endif</td>
                                        <th><a href="{{URL::action('Admin\ContentController@getEditClasscat',$classcat->id)}}"> <button type="button" class="btn btn-success">ويرايش <span class="badge"></span></button></a></th>
                                        <th><a href="{{URL::action('Admin\ContentController@getDeleteClasscat',$classcat->id)}}"> <button type="button" class="btn btn-danger">حـــذف<span class="badge"></span></button></a></th>
                                    </tr>
                                @endforeach()
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection