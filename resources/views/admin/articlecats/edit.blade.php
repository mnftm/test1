@extends('layouts.admin.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

    <form method="post" action="{{URL::action('Admin\ContentController@postEditArticlecat',$data->id)}}" enctype="multipart/form-data" id="rahweb_form" >
        @include('admin.articlecats.form')
    </form>
            </div>
            </div>
            </div>
@endsection