{{csrf_field()}}
<div class="content-wrapper">
    <div class="content-header">
        <div class="container">
                <div class="col-md-12">

<div class="form-group">
    <label>نام اسلايدر</label>
    <input class="form-control" type="text" name="title"
           value="@if(isset($data->title)) {{$data->title}} @endif">
</div>
<div class="form-group">
    <label>توضيحات</label>
    <textarea class="form-control"  name="des">
        @if(isset($data->des)) {{$data->des}} @endif</textarea>
</div>



        <div class="form-group">
            <label for="exampleInputFile">ارسال فایل</label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="exampleInputFile">
                    <label class="custom-file-label" for="exampleInputFile">انتخاب فایل</label>
                </div>
                <div class="input-group-append">
                    <span class="input-group-text" id="">Upload</span>
                </div>
            </div>
        </div>

    </div>
<!-- /.card-body -->

<div class="card-footer">
    <button type="submit" class="btn btn-primary">ذخيره</button>
</div>
</div>

</div>
</div>
</div>





