<!doctype html>
<html lang="fa">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="HTML5,CSS3,HTML,Template,single-page,Single Property,Holsworthy - Single Property HTML Template" >
    <meta name="description" content="Holsworthy - Single Property HTML Template">
    <meta name="author" content="Barat Hadian">

    <!-- Bootstrap Min CSS -->
    <link rel="stylesheet" href="{{asset('assets/site/css/bootstrap.min.css')}}">
    <!-- Animate Min CSS -->
    <link rel="stylesheet" href="{{asset('assets/site/css/animate.min.css')}}">
    <!-- IcoFont Min CSS -->
    <link rel="stylesheet" href="{{asset('assets/site/css/icofont.min.css')}}">
    <!-- Swiper Min CSS -->
    <link rel="stylesheet" href="{{asset('assets/site/css/swiper.min.css')}}">
    <!-- Magnific Popup Min CSS -->
    <link rel="stylesheet" href="{{asset('assets/site/css/magnific-popup.min.css')}}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('assets/site/css/style.css')}}">
    <!-- RTL CSS -->
    <link rel="stylesheet" href="{{asset('assets/site/css/rtl.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('assets/site/css/responsive.css')}}">

    <title>موسسه همگامان با مسير ولايت</title>

    <link rel="icon" type="image/png" href="{{asset('assets/site/img/favicon2.png')}}">
</head>

<body id="home" data-spy="scroll" data-offset="70">

<!-- Start Preloader Area -->
<div class="preloader">
    <div class="loader-gif">
        <img src="{{asset('assets/site/img/preloader.gif')}}" alt="gif">
    </div>
</div>
<!-- End Preloader Area -->

<!-- Start Header Area -->
<header class="header-area">
    <div class="top-header">
        <div class="row align-items-center">
            <div class="col-lg-7">
                <ul class="header-contact">
                    <li><i class="icofont-location-pin"></i> <a href="#">انتهای اشرفی اصفهانی، خيابان طالقانی، خيابان پنجم، پلاك 9</a></li>
                    <li><i class="icofont-phone"></i> <a href="#">021-44803357</a></li>
                </ul>
            </div>

            <div class="col-lg-5">
                <ul class="social">
                    <li><a href="#"><i class="icofont-facebook"></i></a></li>
                    <li><a href="#"><i class="icofont-twitter"></i></a></li>
                    <li><a href="#"><i class="icofont-instagram"></i></a></li>
                    <li><a href="#"><i class="icofont-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Start Navbar Area -->
    <nav class="navbar navbar-expand-lg navbar-style-two navbar-light bg-light">
        <a class="navbar-brand" href="index.html">
            <img src="{{asset('assets/site/img/arm2.png')}}" alt="logo">
            <img src="{{asset('assets/site/img/arm3.png')}}" alt="logo">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item"><a class="nav-link" href="#home">خانه</a></li>
                <li class="nav-item"><a class="nav-link" href="#about">خدمات</a></li>
                <li class="nav-item"><a class="nav-link" href="#details">جزئیات</a></li>
                <li class="nav-item"><a class="nav-link" href="#gallery">نمایشگاه</a></li>
                <li class="nav-item"><a class="nav-link" href="#agent">مشتریان</a></li>
                <li class="nav-item"><a class="nav-link" href="#feedback">بازخورد</a></li>
                <li class="nav-item"><a class="nav-link" href="#services">خدمات</a></li>
                <li class="nav-item"><a class="nav-link" href="#contact">تماس با ما</a></li>
            </ul>

            <ul class="others-options">
                <li><a href="#" class="default-btn">ارسال بازخورد</a></li>
            </ul>
        </div>
    </nav>
    <!-- End Navbar Area -->

</header>
<!-- End Header Area -->



@yield('content')

<!-- Start Footer Area -->
<footer class="footer-area">
    <div class="container">
        <p> طراحی شده توسط گروه طراحی سايت همگامان  <i class="icofont-copyright"></i> <a href="../../../https@www.rtl-theme.com/author/barat/default.htm" target="_blank">Hamgaman Group</a></p>
    </div>
</footer>
<!-- End Footer Area -->

<div class="go-top"><i class="icofont-thin-up"></i></div>

<!-- JQuery Min JS -->
<script src="{{asset('assets/site/js/jquery.min.js')}}"></script>
<!-- Popper Min JS -->
<script src="{{asset('assets/site/js/popper.min.js')}}"></script>
<!-- Bootstrap Min JS -->
<script src="{{asset('assets/site/js/bootstrap.min.js')}}"></script>
<!-- Swiper Min JS -->
<script src="{{asset('assets/site/js/swiper.min.js')}}"></script>
<!-- Magnific Popup Min JS -->
<script src="{{asset('assets/site/js/jquery.magnific-popup.min.js')}}"></script>
<!-- WOW Min JS -->
<script src="{{asset('assets/site/js/wow.min.js')}}"></script>
<!-- Form Validator Min JS -->
<script src="{{asset('assets/site/js/form-validator.min.js')}}"></script>
<!-- Contact Form Min JS -->
<script src="{{asset('assets/site/js/contact-form-script.js')}}></script>
<!-- Map API JS FILES -->
<!-- Holsworthy Map JS FILE -->
<script src="{{asset('assets/site/js/holsworthy-map.js')}}"></script>
<!-- Main JS -->
<script src="{{asset('assets/site/js/main.js')}}"></script>
</body>
</html>