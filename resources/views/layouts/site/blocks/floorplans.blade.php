<!-- Start Floor Plans Area -->
<section class="floor-plans-area ptb-100">
    <div class="container">
        <div class="section-title">
            <h2>طرح های ساختمان</h2>
            <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="tab">
                    <ul class="tabs">
                        <li><a href="#">طبقه 1</a></li>

                        <li><a href="#">طبقه 2</a></li>

                        <li><a href="#">طبقه 3</a></li>

                        <li><a href="#">طبقه 4</a></li>

                        <li><a href="#">طبقه 5</a></li>
                    </ul>

                    <div class="tab_content">
                        <div class="tabs_item">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12 content">
                                    <div class="tabs_item_content">
                                        <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                                        <ul>
                                            <li>شماره طبقه <span>1</span></li>
                                            <li>اتاق ها <span>4</span></li>
                                            <li>کل منطقه <span>311.50 فوت مربع</span></li>
                                            <li>مزایای آشپزخانه <span>بله</span></li>
                                            <li>پنجره ها <span>4</span></li>
                                        </ul>

                                        <a href="#" class="default-btn">درخواست</a>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 image">
                                    <div class="tabs_item_image">
                                        <img src="{{asset('assets/site/img/floor-img1.png')}}" alt="image">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tabs_item">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12 image">
                                    <div class="tabs_item_image">
                                        <img src="{{asset('assets/site/img/floor-img2.png')}}" alt="image">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 content">
                                    <div class="tabs_item_content">
                                        <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                                        <ul>
                                            <li>شماره طبقه <span>1</span></li>
                                            <li>اتاق ها <span>4</span></li>
                                            <li>کل منطقه <span>311.50 فوت مربع</span></li>
                                            <li>مزایای آشپزخانه <span>بله</span></li>
                                            <li>پنجره ها <span>4</span></li>
                                        </ul>

                                        <a href="#" class="default-btn">درخواست</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tabs_item">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12 content">
                                    <div class="tabs_item_content">
                                        <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                                        <ul>
                                            <li>شماره طبقه <span>1</span></li>
                                            <li>اتاق ها <span>4</span></li>
                                            <li>کل منطقه <span>311.50 فوت مربع</span></li>
                                            <li>مزایای آشپزخانه <span>بله</span></li>
                                            <li>پنجره ها <span>4</span></li>
                                        </ul>

                                        <a href="#" class="default-btn">درخواست</a>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 image">
                                    <div class="tabs_item_image">
                                        <img src="{{asset('assets/site/img/floor-img3.png')}}" alt="image">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tabs_item">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12 image">
                                    <div class="tabs_item_image">
                                        <img src="{{asset('assets/site/img/floor-img4.png')}}" alt="image">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 content">
                                    <div class="tabs_item_content">
                                        <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                                        <ul>
                                            <li>شماره طبقه <span>1</span></li>
                                            <li>اتاق ها <span>4</span></li>
                                            <li>کل منطقه <span>311.50 فوت مربع</span></li>
                                            <li>مزایای آشپزخانه <span>بله</span></li>
                                            <li>پنجره ها <span>4</span></li>
                                        </ul>

                                        <a href="#" class="default-btn">درخواست</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tabs_item">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12 content">
                                    <div class="tabs_item_content">
                                        <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                                        <ul>
                                            <li>شماره طبقه <span>1</span></li>
                                            <li>اتاق ها <span>4</span></li>
                                            <li>کل منطقه <span>311.50 فوت مربع</span></li>
                                            <li>مزایای آشپزخانه <span>بله</span></li>
                                            <li>پنجره ها <span>4</span></li>
                                        </ul>

                                        <a href="#" class="default-btn">درخواست</a>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 image">
                                    <div class="tabs_item_image">
                                        <img src="{{asset('assets/site/img/floor-img5.png')}}" alt="image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Floor Plans Area -->