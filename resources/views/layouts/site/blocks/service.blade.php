<!-- Start Services Area -->
<section id="services" class="services-area ptb-100">
    <div class="container">
        <div class="section-title">
            <h2>خدمات ما</h2>
            <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <div class="icon">
                        <i class="icofont-search-property"></i>
                    </div>
                    <h3>مدیریت املاک</h3>
                    <div class="bar"></div>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    <a href="#">ادامه خواندن <i class="icofont-bubble-left"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <div class="icon">
                        <i class="icofont-chart-pie-alt"></i>
                    </div>
                    <h3>مراقبت از سرمایه</h3>
                    <div class="bar"></div>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    <a href="#">ادامه خواندن <i class="icofont-bubble-left"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <div class="icon">
                        <i class="icofont-chart-histogram-alt"></i>
                    </div>
                    <h3>گزارش مالی</h3>
                    <div class="bar"></div>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    <a href="#">ادامه خواندن <i class="icofont-bubble-left"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <div class="icon">
                        <i class="icofont-flora-flower"></i>
                    </div>
                    <h3>توسعه کسب و کار</h3>
                    <div class="bar"></div>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    <a href="#">ادامه خواندن <i class="icofont-bubble-left"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <div class="icon">
                        <i class="icofont-chart-growth"></i>
                    </div>
                    <h3>وام مسکن</h3>
                    <div class="bar"></div>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    <a href="#">ادامه خواندن <i class="icofont-bubble-left"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <div class="icon">
                        <i class="icofont-money"></i>
                    </div>
                    <h3>بازیابی دارایی</h3>
                    <div class="bar"></div>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    <a href="#">ادامه خواندن <i class="icofont-bubble-left"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Services Area -->