<!-- Start About Area -->
<section id="about" class="about-area ptb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-12">
                <div class="about-image">
                    <img src="{{asset('assets/site/img/about-img1.jpg')}}" alt="about-image">
                    <img src="{{asset('assets/site/img/layer.png')}}" class="back-img2" alt="layer">
                    <img src="{{asset('assets/site/img/circle-img.jpg')}}" class="circle-img" alt="circle-image">
                </div>
            </div>

            <div class="col-lg-7 col-md-12">
                <div class="about-content">
                    <h3>خدمات موسسه همگامان</h3>
                    <p></p>

                    <div class="about-inner">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-building-alt"></i>
                                    </div>
                                    <p><br></p>
                                    <h4>طراحی سايت</h4>
                                    <p></p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-bed"></i>
                                    </div>
                                    <p><br></p>
                                    <h4>عكاسی</h4>
                                    <p><br></p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-bathtub"></i>
                                    </div>
                                    <p><br></p>
                                    <h4>سخت‌افزار</h4>
                                    <p><br></p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-beach-bed"></i>
                                    </div>
                                    <p><br></p>
                                    <h4>نرم‌افزار</h4>
                                    <p><br></p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-car-alt-4"></i>
                                    </div>
                                    <p><br></p>
                                    <h4>شبكه</h4>
                                    <p><br></p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-building"></i>
                                    </div>
                                    <p><br></p>
                                    <h4>طراحی</h4>
                                    <p><br></p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-hotel"></i>
                                    </div>
                                    <p><br></p>
                                    <h4>تايپ و تكثير</h4>
                                    <p><br></p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-island"></i>
                                    </div>
                                    <p><br></p>
                                    <h4>چاپ عكس</h4>
                                    <p><br></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About Area -->