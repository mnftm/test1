<!-- Start Main Banner -->
<section class="home-area">
    <div class="swiper-container home-slides">
        <div class="swiper-wrapper">
            <div class="swiper-slide main-banner item-bg1">
                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="main-banner-text">
                            <h1>مشاوره<span></span></h1>
                            <p>وب‌سايت اختصاصی مشاوره</p>

                            <a href="#" class="default-btn">مشاهده جزئیات</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper-slide main-banner item-bg2">
                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="main-banner-text">
                            <h1> LMS <span></span></h1>
                            <p>آموزش مجازی آنلاين</p>

                            <a href="#" class="default-btn">مشاهده جزئیات</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="swiper-slide main-banner item-bg3">
                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="main-banner-text">
                            <h1>خدمات<span></span></h1>
                            <p>ارائه خدمات گوناگون</p>

                            <a href="#" class="default-btn">مشاهده جزئیات</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="swiper-pagination"></div>
    </div>
</section>
<!-- End Main Banner -->