<!-- Start Feedback Area -->
<section id="feedback" class="feedback-area ptb-100">
    <div class="container">
        <div class="section-title">
            <h2>بازخورد مشتری</h2>
            <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-feedback">
                    <div class="client-image">
                        <img src="{{asset('assets/site/img/client1.jpg')}}" alt="image">
                    </div>
                    <ul class="rating">
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                    </ul>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                    <div class="bar"></div>
                    <h3>جان اسمیت</h3>
                    <span>مدیر موسسه</span>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-feedback">
                    <div class="client-image">
                        <img src="{{asset('assets/site/img/client2.jpg')}}" alt="image">
                    </div>
                    <ul class="rating">
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                    </ul>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                    <div class="bar"></div>
                    <h3>جان اسمیت</h3>
                    <span>مدیر موسسه</span>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 offset-lg-0 offset-md-3">
                <div class="single-feedback">
                    <div class="client-image">
                        <img src="{{asset('assets/site/img/client3.jpg')}}" alt="image">
                    </div>
                    <ul class="rating">
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                    </ul>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                    <div class="bar"></div>
                    <h3>جان اسمیت</h3>
                    <span>مدیر موسسه</span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Feedback Area -->