<!-- Start Property Details Area -->
<section id="details" class="property-details-area">
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="col-lg-6 col-md-12 p-0">
                <div class="video-image">
                    <img src="{{asset('assets/site/img/video-bg.jpg')}}" alt="image">

                </div>
            </div>

            <div class="col-lg-6 col-md-12 p-0">
                <div class="property-details-info">
                    <div class="property-details">
                        <h3>جزئیات روشن هستند</h3>

                        <ul>
                            <li>نوع منو: <span>یک خانواده</span></li>
                            <li>شناسه منو: <span>5566128</span></li>
                            <li>اتاق های خواب: <span>5</span></li>
                            <li>حمام: <span>بله</span></li>
                            <li>اندازه متراژ: <span>7،965 فوت مربع </span></li>
                            <li>اندازه بزرگ: <span>8،276.00 فوت مربع </span></li>
                            <li>وضعیت: <span>فعال است</span></li>
                            <li>سال ساخت: <span>1395</span></li>
                            <li>مقیاس: <span>18252.0</span></li>
                            <li>سال مالیاتی: <span>2018</span></li>
                        </ul>
                    </div>

                    <div class="additional-details">
                        <h3>جزئیات بیشتر</h3>

                        <ul>
                            <li>سوخت: <span>برق و گاز</span></li>
                            <li>نوع ساخت و ساز: <span>آجر، قاب</span></li>
                            <li>سایت شومینه: <span>اتاق خواب</span></li>
                            <li>سقف: <span>ترکیب کمربند</span></li>
                            <li>بخاری: <span>چوب</span></li>
                            <li>اندازه بزرگ: <span>8،276.00 فوت مربع</span></li>
                            <li>ویژگی های داخلی: <span>آشپزخانه</span></li>
                            <li>سطح جاده: <span>جاده آسفالت</span></li>
                            <li>فروش اجاره: <span>برای فروش</span></li>
                            <li>فروشی ملکی: <span>در انتظار فروش</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Property Details Area -->
