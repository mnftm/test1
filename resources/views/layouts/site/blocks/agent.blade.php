<!-- Start Agent Area -->
<section id="agent" class="agent-area ptb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-12">
                <div class="agent-image">
                    <img src="{{asset('assets/site/img/agent.jpg')}}" alt="agent">
                    <img src="{{asset('assets/site/img/layer.png')}}" class="back-img1" alt="layer">
                    <img src="{{asset('assets/site/img/layer.png')}}" class="back-img2" alt="layer">
                </div>
            </div>

            <div class="col-lg-7 col-md-12">
                <div class="agent-content">
                    <div class="section-title">
                        <h2>گالری عکس</h2>
                        <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    </div>

                    <div class="agent-info">
                        <h3>بریان آدامز</h3>
                        <span>گالری عکس</span>
                    </div>

                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                    <ul class="agent-contact-info">
                        <li>
                            <span>دفتر مرکزی:</span>
                            <i class="icofont-phone-circle"></i>
                            <a href="#">+444 157 895 4578</a>
                        </li>
                        <li>
                            <span>دفتر:</span>
                            <i class="icofont-fax"></i>
                            <a href="#">+444 157 895 4578</a>
                        </li>
                        <li>
                            <span>ایمیل:</span>
                            <i class="icofont-envelope"></i>
                            <a href="#">Yoursite@Mail.Com</a>
                        </li>
                        <li>
                            <span>نمابر:</span>
                            <i class="icofont-envelope-open"></i>
                            <a href="#">+444 157 895 4578</a>
                        </li>
                    </ul>

                    <ul class="social">
                        <li><a href="#"><i class="icofont-facebook"></i></a></li>
                        <li><a href="#"><i class="icofont-twitter"></i></a></li>
                        <li><a href="#"><i class="icofont-linkedin"></i></a></li>
                        <li><a href="#"><i class="icofont-instagram"></i></a></li>
                    </ul>

                    <a href="#" class="default-btn">تماس بگیرید</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Agent Area -->