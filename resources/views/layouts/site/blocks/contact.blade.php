<!-- Start Contact Area -->
<section id="contact" class="contact-area ptb-100">
    <div class="container">
        <div class="section-title">
            <h2>تماس بگیرید</h2>
            <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
        </div>

        <div class="row">
            <div class="col-lg-5 col-md-12">
                <div class="contact-box">
                    <div class="agent-contact">
                        <div class="image">
                            <img src="{{asset('assets/site/img/contact-agent.jpg')}}" alt="image">
                        </div>

                        <div class="content">
                            <h3>بریان آدامز</h3>
                            <span>مدیر فروش</span>
                            <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                        </div>
                    </div>

                    <ul class="contact-info">
                        <li><i class="icofont-phone-circle"></i> <a href="#">+444 157 895 4578</a></li>
                        <li><i class="icofont-fax"></i> <a href="#">+444 157 895 4578</a></li>
                        <li><i class="icofont-envelope"></i> <a href="#">Yoursite@Mail.Com</a></li>
                        <li><i class="icofont-envelope-open"></i> <a href="#">+444 157 895 4578</a></li>
                    </ul>

                    <ul class="social">
                        <li><a href="#"><i class="icofont-facebook"></i></a></li>
                        <li><a href="#"><i class="icofont-twitter"></i></a></li>
                        <li><a href="#"><i class="icofont-linkedin"></i></a></li>
                        <li><a href="#"><i class="icofont-instagram"></i></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-7 col-md-12">
                <form id="contactForm">
                    <div class="row">
                        <div class="col-lg-12 col-md-6">
                            <div class="form-group">
                                <label>نام شما</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="نام خود را وارد کنید" required data-error="لطفا نام خود را وارد کنید">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-6">
                            <div class="form-group">
                                <label>آدرس ایمیل شما</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="آدرس ایمیل خود را وارد کنید" required data-error="لطفا آدرس ایمیل خود را وارد کنید">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>موضوع شما</label>
                                <input type="text" class="form-control" name="msg_subject" id="msg_subject" required data-error="موضوع خود را وارد کنید" placeholder="موضوع خود را وارد کنید">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>پیام شما</label>
                                <textarea placeholder="پیام خود را وارد کنید" name="message" id="message" class="form-control" cols="30" rows="4" required data-error="پیام خود را بنویسید"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <button type="submit" class="default-btn">ارسال پیام</button>
                            <div id="msgSubmit" class="h3 text-center hidden"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- End Contact Area -->