<!-- Start Photo Gallery Area -->
<section id="gallery" class="photo-gallery-area ptb-100 pb-0">
    <div class="container">
        <div class="section-title">
            <h2>گالری عکس</h2>
            <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
        </div>
    </div>

    <div class="row m-0">
        <div class="col-lg-6 col-md-12 p-0">
            <div class="photo-gallery-item">
                <a href="{{asset('assets/site/img/gallery-img1.jpg')}}" class="popup-btn"><img src="{{asset('assets/site/img/gallery-img1.jpg')}}" alt="image"></a>
            </div>
        </div>

        <div class="col-lg-6 col-md-12 p-0">
            <div class="row m-0">
                <div class="col-lg-6 col-sm-6 col-md-6 p-0">
                    <div class="photo-gallery-item">
                        <a href="{{asset('assets/site/img/gallery-img2.jpg')}}" class="popup-btn"><img src="{{asset('assets/site/img/gallery-img2.jpg')}}" alt="image"></a>
                    </div>
                </div>

                <div class="col-lg-6 col-sm-6 col-md-6 p-0">
                    <div class="photo-gallery-item">
                        <a href="{{asset('assets/site/img/gallery-img3.jpg')}}" class="popup-btn"><img src="{{asset('assets/site/img/gallery-img3.jpg')}}" alt="image"></a>
                    </div>
                </div>

                <div class="col-lg-12 col-sm-12 col-md-12 p-0">
                    <div class="photo-gallery-item">
                        <a href="{{asset('assets/site/img/gallery-img4.jpg')}}" class="popup-btn"><img src="{{asset('assets/site/img/gallery-img4.jpg')}}" alt="image"></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-12 p-0">
            <div class="photo-gallery-item">
                <a href="{{asset('assets/site/img/gallery-img5.jpg')}}" class="popup-btn"><img src="{{asset('assets/site/img/gallery-img5.jpg')}}" alt="image"></a>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6 col-md-6 p-0">
            <div class="photo-gallery-item">
                <a href="{{asset('assets/site/img/gallery-img6.jpg')}}" class="popup-btn"><img src="{{asset('assets/site/img/gallery-img6.jpg')}}" alt="image"></a>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6 col-md-6 p-0">
            <div class="photo-gallery-item">
                <a href="{{asset('assets/site/img/gallery-img7.jpg')}}" class="popup-btn"><img src="{{asset('assets/site/img/gallery-img7.jpg')}}" alt="image"></a>
            </div>
        </div>
    </div>
</section>
<!-- End Photo Gallery Area -->