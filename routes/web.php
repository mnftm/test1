<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/', function () {
        return view('site.index');
});

Route::get('/admin', function () {
    return view('admin.index');
});


Route::middleware('auth')->namespace('Admin')->prefix('admin')->group(function () {
/** slider **/
Route::get('/sliders' , 'ContentController@getSlider');
Route::get('/sliders/add' , 'ContentController@getAddSlider');
Route::post('/sliders/add' , 'ContentController@postAddSlider');
Route::get('/sliders/edit/{id}' , 'ContentController@getEditSlider');
Route::post('/sliders/edit/{id}' , 'ContentController@postEditSlider');
Route::get('/sliders/delete/{id}' , 'ContentController@getDeleteSlider');
/**End slider **/

/** service **/
Route::get('/services' , 'ContentController@getService');
Route::get('/services/add' , 'ContentController@getAddService');
Route::post('/services/add' , 'ContentController@postAddService');
Route::get('/services/edit/{id}' , 'ContentController@getEditService');
Route::post('/services/edit/{id}' , 'ContentController@postEditService');
Route::get('/services/delete/{id}' , 'ContentController@getDeleteService');
/**End service **/

/** notice **/
Route::get('/notices' , 'ContentController@getNotice');
Route::get('/notices/add' , 'ContentController@getAddNotice');
Route::post('/notices/add' , 'ContentController@postAddNotice');
Route::get('/notices/edit/{id}' , 'ContentController@getEditNotice');
Route::post('/notices/edit/{id}' , 'ContentController@postEditNotice');
Route::get('/notices/delete/{id}' , 'ContentController@getDeleteNotice');
/**End notice **/

/** customer **/
Route::get('/customers' , 'ContentController@getCustomer');
Route::get('/customers/add' , 'ContentController@getAddCustomer');
Route::post('/customers/add' , 'ContentController@postAddCustomer');
Route::get('/customers/edit/{id}' , 'ContentController@getEditCustomer');
Route::post('/customers/edit/{id}' , 'ContentController@postEditCustomer');
Route::get('/customers/delete/{id}' , 'ContentController@getDeleteCustomer');
/**End customer **/

/** class **/
Route::get('/classes' , 'ContentController@getClass');
Route::get('/classes/add' , 'ContentController@getAddClass');
Route::post('/classes/add' , 'ContentController@postAddClass');
Route::get('/classes/edit/{id}' , 'ContentController@getEditClass');
Route::post('/classes/edit/{id}' , 'ContentController@postEditClass');
Route::get('/classes/delete/{id}' , 'ContentController@getDeleteClass');
/**End class **/

/** classcat **/
Route::get('/classcats' , 'ContentController@getClasscat');
Route::get('/classcats/add' , 'ContentController@getAddClasscat');
Route::post('/classcats/add' , 'ContentController@postAddClasscat');
Route::get('/classcats/edit/{id}' , 'ContentController@getEditClasscat');
Route::post('/classcats/edit/{id}' , 'ContentController@postEditClasscat');
Route::get('/classcats/delete/{id}' , 'ContentController@getDeleteClasscat');
/**End classcat **/

/** article **/
Route::get('/articles' , 'ContentController@getArticle');
Route::get('/articles/add' , 'ContentController@getAddArticle');
Route::post('/articles/add' , 'ContentController@postAddArticle');
Route::get('/articles/edit/{id}' , 'ContentController@getEditArticle');
Route::post('/articles/edit/{id}' , 'ContentController@postEditArticle');
Route::get('/articles/delete/{id}' , 'ContentController@getDeleteArticle');
/**End article **/

/** articlecat **/
Route::get('/articlecats' , 'ContentController@getArticlecat');
Route::get('/articlecats/add' , 'ContentController@getAddArticlecat');
Route::post('/articlecats/add' , 'ContentController@postAddArticlecat');
Route::get('/articlecats/edit/{id}' , 'ContentController@getEditArticlecat');
Route::post('/articlecats/edit/{id}' , 'ContentController@postEditArticlecat');
Route::get('/articlecats/delete/{id}' , 'ContentController@getDeleteArticlecat');
/**End articlecat **/

});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
