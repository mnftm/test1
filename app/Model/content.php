<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class content extends Model
{
    protected $table = 'content';
    use SoftDeletes;
    protected $fillable=['title','type','des','img','status','order','link','parent'];
}
