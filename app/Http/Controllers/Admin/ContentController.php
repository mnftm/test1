<?php

namespace App\Http\Controllers\Admin;

use App\Model\Content;
use Classes\UploadImg;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ContentController extends Controller
{

/** slider **/
    public function getSlider(){
        $slider=Content::whereType(1)->get();
        return View('admin.sliders.index')
            ->with('sliders',$slider);
    }

    public function getAddSlider(){
        return view('admin.sliders.add');
    }

    public function postAddSlider(Request $request){
        $input=$request->all();
        $input["type"]='1';
        if ($request->hasFile('img')) {
            $path = "assets/admin/uploads";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('img'), $path);
            $input['img'] = $fileName;
        }
        $slider=Content::create($input);
        return Redirect::action('Admin\ContentController@getSlider');
    }

    public function getEditSlider($id){
        $data=Content::whereType(1)->findorfail($id);
        return View('admin.sliders.edit')
            ->with('data',$data);
    }

    public function postEditSlider($id,Request $request){
        $input = $request->all();
        $slider=content::wheretype(1)->find($id);
        if ($request->hasFile('image')){
        }
        $slider->update($input);
        return Redirect::action('Admin\ContentController@getSlider');
    }

    public function getDeleteSlider($id){
        content::destroy($id);
        return Redirect::action('Admin\ContentController@getSlider');
    }
/**End slider **/

/** service **/
    public function getService(){
        $service=Content::whereType(2)->get();
        return View('admin.services.index')
            ->with('services',$service);
    }

    public function getAddService(){
        return view('admin.services.add');
    }

    public function postAddService(Request $request){
        $input=$request->all();
        $input["type"]='2';
        if ($request->hasFile('img')) {
            $path = "assets/admin/uploads";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('img'), $path);
            $input['img'] = $fileName;
        }
        $service=Content::create($input);
        return Redirect::action('Admin\ContentController@getService');
    }

    public function getEditService($id){
        $data=Content::whereType(2)->findorfail($id);
        return View('admin.services.edit')
            ->with('data',$data);
    }

    public function postEditService($id,Request $request){
        $input = $request->all();
        $service=content::wheretype(2)->find($id);
        if ($request->hasFile('image')){
        }
        $service->update($input);
        return Redirect::action('Admin\ContentController@getService');
    }

    public function getDeleteService($id){
        content::destroy($id);
        return Redirect::action('Admin\ContentController@getService');
    }
/**End service **/

/** Notice **/
    public function getNotice(){
        $notice=Content::whereType(3)->get();
        return View('admin.notices.index')
            ->with('notices',$notice);
    }

    public function getAddNotice(){
        return view('admin.notices.add');
    }

    public function postAddNotice(Request $request){
        $input=$request->all();
        $input["type"]='3';
        if ($request->hasFile('img')) {
            $path = "assets/admin/uploads";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('img'), $path);
            $input['img'] = $fileName;
        }
        $notice=Content::create($input);
        return Redirect::action('Admin\ContentController@getNotice');
    }

    public function getEditNotice($id){
        $data=Content::whereType(3)->findorfail($id);
        return View('admin.notices.edit')
            ->with('data',$data);
    }

    public function postEditNotice($id,Request $request){
        $input = $request->all();
        $notice=content::wheretype(3)->find($id);
        if ($request->hasFile('image')){
        }
        $notice->update($input);
        return Redirect::action('Admin\ContentController@getNotice');
    }

    public function getDeleteNotice($id){
        content::destroy($id);
        return Redirect::action('Admin\ContentController@getNotice');
    }
/**End Notice **/

/** Customer **/
    public function getCustomer(){
        $customer=Content::whereType(4)->get();
        return View('admin.customers.index')
            ->with('customers',$customer);
    }

    public function getAddCustomer(){
        return view('admin.customers.add');
    }

    public function postAddCustomer(Request $request){
        $input=$request->all();
        $input["type"]='4';
        if ($request->hasFile('img')) {
            $path = "assets/admin/uploads";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('img'), $path);
            $input['img'] = $fileName;
        }
        $customer=Content::create($input);
        return Redirect::action('Admin\ContentController@getCustomer');
    }

    public function getEditCustomer($id){
        $data=Content::whereType(4)->findorfail($id);
        return View('admin.customers.edit')
            ->with('data',$data);
    }

    public function postEditCustomer($id,Request $request){
        $input = $request->all();
        $customer=content::wheretype(4)->find($id);
        if ($request->hasFile('image')){
        }
        $customer->update($input);
        return Redirect::action('Admin\ContentController@getCustomer');
    }

    public function getDeleteCustomer($id){
        content::destroy($id);
        return Redirect::action('Admin\ContentController@getCustomer');
    }
/**End Customer **/

/** Class **/
    public function getClass(){
        $class=Content::whereType(5)->get();
        return View('admin.classes.index')
            ->with('classes',$class);
    }

    public function getAddClass(){
        return view('admin.classes.add');
    }

    public function postAddClass(Request $request){
        $input=$request->all();
        $input["type"]='5';
        if ($request->hasFile('img')) {
            $path = "assets/admin/uploads";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('img'), $path);
            $input['img'] = $fileName;
        }
        $class=Content::create($input);
        return Redirect::action('Admin\ContentController@getClass');
    }

    public function getEditClass($id){
        $data=Content::whereType(5)->findorfail($id);
        return View('admin.classes.edit')
            ->with('data',$data);
    }

    public function postEditClass($id,Request $request){
        $input = $request->all();
        $class=content::wheretype(5)->find($id);
        if ($request->hasFile('image')){
        }
        $class->update($input);
        return Redirect::action('Admin\ContentController@getClass');
    }

    public function getDeleteClass($id){
        content::destroy($id);
        return Redirect::action('Admin\ContentController@getClass');
    }
/**End Class **/

/** Classcat **/
    public function getClasscat(){
        $classcat=Content::whereType(6)->get();
        return View('admin.classcats.index')
            ->with('classcats',$classcat);
    }

    public function getAddClasscat(){
        return view('admin.classcats.add');
    }

    public function postAddClasscat(Request $request){
        $input=$request->all();
        $input["type"]='6';
        if ($request->hasFile('img')) {
            $path = "assets/admin/uploads";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('img'), $path);
            $input['img'] = $fileName;
        }
        $classcat=Content::create($input);
        return Redirect::action('Admin\ContentController@getClasscat');
    }

    public function getEditClasscat($id){
        $data=Content::whereType(6)->findorfail($id);
        return View('admin.classcats.edit')
            ->with('data',$data);
    }

    public function postEditClasscat($id,Request $request){
        $input = $request->all();
        $classcat=content::wheretype(6)->find($id);
        if ($request->hasFile('image')){
        }
        $classcat->update($input);
        return Redirect::action('Admin\ContentController@getClasscat');
    }

    public function getDeleteClasscat($id){
        content::destroy($id);
        return Redirect::action('Admin\ContentController@getClasscat');
    }
/**End Classcat **/

/** Article **/
    public function getArticle(){
        $article=Content::whereType(7)->get();
        return View('admin.articles.index')
            ->with('articles',$article);
    }

    public function getAddArticle(){
        return view('admin.articles.add');
    }

    public function postAddArticle(Request $request){
        $input=$request->all();
        $input["type"]='7';
        if ($request->hasFile('img')) {
            $path = "assets/admin/uploads";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('img'), $path);
            $input['img'] = $fileName;
        }
        $article=Content::create($input);
        return Redirect::action('Admin\ContentController@getArticle');
    }

    public function getEditArticle($id){
        $data=Content::whereType(7)->findorfail($id);
        return View('admin.articles.edit')
            ->with('data',$data);
    }

    public function postEditArticle($id,Request $request){
        $input = $request->all();
        $article=content::wheretype(7)->find($id);
        if ($request->hasFile('image')){
        }
        $article->update($input);
        return Redirect::action('Admin\ContentController@getArticle');
    }

    public function getDeleteArticle($id){
        content::destroy($id);
        return Redirect::action('Admin\ContentController@getArticle');
    }
/**End Article **/

/** Articlecat **/
    public function getArticlecat(){
        $articlecat=Content::whereType(8)->get();
        return View('admin.articlecats.index')
            ->with('articlecats',$articlecat);
    }

    public function getAddArticlecat(){
        return view('admin.articlecats.add');
    }

    public function postAddArticlecat(Request $request){
        $input=$request->all();
        $input["type"]='8';
        if ($request->hasFile('img')) {
            $path = "assets/admin/uploads";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('img'), $path);
            $input['img'] = $fileName;
        }
        $article=Content::create($input);
        return Redirect::action('Admin\ContentController@getArticlecat');
    }

    public function getEditArticlecat($id){
        $data=Content::whereType(8)->findorfail($id);
        return View('admin.articlecats.edit')
            ->with('data',$data);
    }

    public function postEditArticlecat($id,Request $request){
        $input = $request->all();
        $articlecat=content::wheretype(8)->find($id);
        if ($request->hasFile('image')){
        }
        $articlecat->update($input);
        return Redirect::action('Admin\ContentController@getArticlecat');
    }

    public function getDeleteArticlecat($id){
        content::destroy($id);
        return Redirect::action('Admin\ContentController@getArticlecat');
    }
/**End Articlecat **/







}
