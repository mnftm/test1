<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function getLogin()
    {

        if (Auth::check() and Auth::user()->admin)
            return Redirect::action('Admin\HomeController@getIndex');
        else
            return view('admin.auth.login');
    }

    public function postLogin(AuthRequest $request)
    {
        $login = Auth::attempt([
            'admin' => 1,
            'status' => 1,
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ]);

        if ($login) {
            return Redirect::action('Admin\HomeController@getIndex');
        }
        return Redirect::action('Auth\LoginController@getLogin')
            ->with('error', 'اطلاعات ورود اشتباه می باشد.');
    }

    public function postLoginUser(Request $request)
    {
        $login = Auth::attempt([
            'admin' => 0,
            'mobile' => $request->get('mobile'),
            'password' => $request->get('password')
        ]);

        if ($login) {
            return Redirect::back()->with('msg', 'login success');
        }
        return  Redirect::back()->with('error', 'اطلاعات ورود اشتباه می باشد.');
    }
}
