<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class PanelPermission
{

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        $segments = $request->segments();
        if ($this->auth->check()) {
            if ($this->auth->user()->admin == 0) {
                if ($segments[0] == Config::get('site.panel')) {
                    return $next($request);
                }
            }
        }
        if ($request->ajax()) {
            return response('Unauthorized.', 401);
        } else {
            if (Auth::check() and $this->auth->user()->admin == 0) {
                return redirect('/' . config('site.panel'))->with('error', 'شما به این بخش دسترسی ندارید.');
            } else {
                return redirect()->action('Site\HomeController@getIndex');
            }
        }
    }
}
